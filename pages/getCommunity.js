import React, { useEffect, useState }  from 'react';
import Image from 'next/image';
import Breadcrumb from '../components/Breadcrumb';
import Link from 'next/link';
import BannerButtons from '../components/BannerButtons';
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import { Pagination } from "swiper";
import Layout from '../components/Layout';

function GetDoctor() {
    // SLIDER SETTINGS
    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        autoplay: false
    }

    // GET get DOCTOR DATA
    const getContent = () => fetch('getCommunity.json').then((resp) => resp.json());
    
    const [content, setContent] = useState({})
    const onLoadContent = async () => {
      const data = await getContent()
      setContent(data.data)
    }
    const highlightImage = content.secondary_image
  
    useEffect(() => {
      onLoadContent()
    }, [])

    return (
        <>
        <Layout title={content.page_title} breadcrumb={content.breadcrumb}>
            <div className="mx-auto container">    
                
                <section>
                    <div className="relative mix-blend-overlay">
                        <div className="background-overlay background-overlay flex items-center">
                            <BannerButtons content={content} />
                        </div>
                        {content.banner_image_path && (
                            <Image src={content.banner_image_path} width="1512" height="600" alt={content.page_title} className="homepage-banner"  />
                        )}
                    </div>
                </section>

                <section className="md:py-14 py-6 flex items-center gap-14 justify-start">
                    <div className='w-1/3 md:block hidden'>
                        {highlightImage && (
                            <Image src={highlightImage} width={1} height={1} layout="responsive" objectFit='cover' alt="community around you" />
                        )}
                    </div>
                    <div className='md:px-12 px-4'>
                        <h3 className="font-bold text-3xl pt-5 pb-2">{content.secondary_title}</h3>
                        <p className='text-md pb-3 md:text-xl text-lg'>{content.secondary_description}</p>
                    </div>
                </section>

                <section className='md:py-14 py-6 xl:px-0 px-4'>
                    <h3 className="font-bold text-3xl pt-5 pb-2">{content.coach?.title}</h3>
                    <div className='md:flex block justify-between items-center'>
                        <p className='text-md pb-3 md:text-xl text-lg md:w-3/4 w-full'>{content.coach?.coaches_description}</p>
                        <Link href="/events" className="color-primary">Lihat Semua</Link>
                    </div>
                    <div className='horizontal-swiper-wrapper'>
                        <Swiper
                            breakpoints={{
                                360: {
                                    width: 360,
                                    slidesPerView: 1.5,
                                },
                                768: {
                                    width: 768,
                                    slidesPerView: 2.5,
                                },
                                1024: {
                                    width: 1024,
                                    slidesPerView: 3.5,
                                },
                            }}
                            // slidesPerView={3.5}
                            spaceBetween={30}
                            pagination={{
                              clickable: true,
                            }}
                            modules={[Pagination]}
                            className="mySwiper"
                        >
                            {content.coach?.coaches?.map((item) => (
                                <SwiperSlide key={item.id}>
                                    <div className="w-full bg-white xl:rounded-3xl rounded-xl drop-shadow-md">
                                        <Image className='rounded-xl' src={item.image_path} width={2} height={1} alt={item.title} layout="responsive" objectFit='cover' />
                                        <div className='py-3 px-4'>
                                            <h4>{item.title}</h4>
                                            <div className='flex gap-4 py-3'>
                                                <Image src={item.image_profile} width="48" height="48" alt={item.name} />
                                                <div>
                                                    <p>{item.name}</p>
                                                    <p>{item.role}</p>
                                                </div>
                                            </div>
                                            <div className='p-3 bg-primary rounded-xl my-3 text-center text-white'>{item.time}</div>
                                        </div>
                                    </div>
                                </SwiperSlide>
                            ))}
                        </Swiper>
                    </div>
                </section>

                <section className="md:py-14 py-6 bg-primary md:mb-10 mb-12 rounded-3xl xl:px-24 lg:px-12 px-4">
                    <h4 className='text-center text-white font-bold text-3xl xl:pb-8'>{content.event?.community_event.title}</h4>
                    <div className='xl:flex md:block items-center justify-between gap-10'>
                        <div className='xl:w-2/3 lg:w-full xl:py-0 py-6'>
                            <Image src={content.banner_image_path} width={3} height={2} layout="responsive" objectFit='cover' alt={content.title} className="rounded-tr-3xl rounded-bl-3xl rounded-tl-xl rounded-br-xl" />
                        </div>
                        <div className='bg-teal-50 rounded-3xl p-8 xl:w-1/3 w-full'>
                            <h4 className='font-semibold md:text-2xl text-xl text-cyan-800 pb-4'>{content.event?.community_event?.subtitle}</h4>
                            <p className='text-lg'>{content.event?.community_event?.description}</p>
                            <div className='xl:flex lg:flex md:block xl:gap-0 md:gap-4 gap-4 justify-between pt-4'>
                                {content.event?.buttons.map((item) => (
                                    <button key={item.id} className={
                                        item.type === 'registration' ? 
                                        'px-5 py-2 bg-primary text-white rounded-lg xl:mb-0 lg:mb-4 mb-3 xl:w-auto sm:w-full w-full' : 
                                        'px-5 py-2 bg-none border-solid border-2 border-primary rounded-lg color-primary xl:w-auto sm:w-full w-full xl:mb-0 lg:mb-4 mb-3'
                                    }>{item.title}</button>
                                ))}
                            </div>
                        </div>
                    </div>
                </section>

                <section className='md:py-14 py-6 xl:px-0 px-4'>
                    <h3 className="font-bold text-3xl pt-5 pb-2">{content.event?.upcoming_events.title}</h3>
                    <div className='md:flex block justify-between items-center'>
                        <p className='text-md pb-3 md:text-xl text-lg md:w-3/4 w-full'>{content.event?.upcoming_events?.event_description}</p>
                        <Link href="/events" className="color-primary">Lihat Semua</Link>
                    </div>
                    <div className='horizontal-swiper-wrapper'>
                        <Swiper
                            breakpoints={{
                                360: {
                                    width: 360,
                                    slidesPerView: 1.5,
                                },
                                768: {
                                    width: 768,
                                    slidesPerView: 2.5,
                                },
                                1024: {
                                    width: 1024,
                                    slidesPerView: 3.5,
                                },
                            }}
                            spaceBetween={30}
                            pagination={{
                              clickable: true,
                            }}
                            modules={[Pagination]}
                            className="mySwiper"
                        >
                            {content.event?.upcoming_events?.events.map((item) => (
                                <SwiperSlide key={item.id}>
                                    <div className="w-full bg-white xl:rounded-3xl rounded-xl drop-shadow-md">
                                        <Image className='rounded-xl' src={item.image_path} width={2} height={1} alt={item.title} layout="responsive" objectFit='cover' />
                                    </div>
                                </SwiperSlide>
                            ))}
                        </Swiper>
                    </div>
                </section>
            </div>
        </Layout>
        </>
    );
}

export default GetDoctor;