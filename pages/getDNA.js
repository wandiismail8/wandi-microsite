import React, { useEffect, useState }  from 'react';
import Image from 'next/image';
import Breadcrumb from '../components/Breadcrumb';
import ProcessBooking from '../components/ProcessBooking';
import Modal from '../components/Modal';
import BannerButtons from '../components/BannerButtons';
import Layout from '../components/Layout';

function GetDNA() {
    // GET DATA
    const getContent = () => fetch('getDNA.json').then((resp) => resp.json());
    
    const [content, setContent] = useState({})
    const onLoadContent = async () => {
      const data = await getContent()
      setContent(data.data)
    }
    const highlightImage2 = content.highlight_2?.background_image_path
  
    useEffect(() => {
      onLoadContent()
    }, [])

    // Modal
    const [showModal, setShowModal] = useState(false)
    const [selectedItem, setSelectedItem] = useState(null)
    const handleSelectContent = (item) => {
        setShowModal(true)
        setSelectedItem(item)
    }

    return (
        <>

        <Layout title={content.page_title} breadcrumb={content.breadcrumb}>
            <div className="mx-auto container">    
                
                <section>
                    <div className="relative mix-blend-overlay">
                        <div className="background-overlay flex items-center">
                            <BannerButtons content={content} />
                        </div>
                        {content.banner_image_path && (
                            <Image src={content.banner_image_path} width="1512" height="600" alt={content.page_title} className="homepage-banner"  />
                        )}
                    </div>
                </section>

                <section className="py-14">
                    <h3 className="font-bold text-center text-xl pt-5 pb-2">{content.highlight_1?.title}</h3>
                    <p className='text-center text-md pb-3'>{content.highlight_1?.description}</p>
                    <div className="flex gap-4 justify-center py-4">
                        {content.highlight_1?.dna_services?.map((item) => (
                            <div onClick={() => { handleSelectContent(item) }} key={item.id} className="cursor-pointer card shadow-md border-solid rounded-xl p-4 max-w-xl" style={{ background: item.background }}>
                                <div>
                                    <Image src={item.image_path} width="120" height="48" alt={item.title} className="rounded-2xl" />
                                </div>
                                <div>
                                    <h4 className="font-bold py-3 text-open-sans">{item.title} <span className='superscript'>{item.superscript}</span></h4>
                                    <p className="pb-2 text-open-sans text-gray-600">{item.short_description}</p>
                                </div>
                                <button className='bg-primary py-2 px-6 text-white rounded-full'>See More</button>
                            </div>
                        ))}
                    </div>
                </section>
            </div>
            <section className='flex items-center justify-between md:mb-0 mb-10'>
                <div className='flex justify-center w-full'>
                    <div className='max-w-2xl px-4'>
                        <h3 className='md:text-3xl text-xl md:font-semibold font-bold py-3 md:leading-10'>{content.highlight_2?.title}</h3>
                        <p className='mb-6 md:text-xl'>{content.highlight_2?.description}</p>
                        <a href='#' className='py-2 px-4 bg-primary rounded-md text-white hover:bg-teal-600 duration-700'>Install Aplikasi</a>
                    </div>
                </div>
                
                {highlightImage2 && (
                    <Image src={highlightImage2} width="500" height="800" alt='get Doctor' className='md:block hidden' />
                )}
            </section>

            <ProcessBooking processBooking={content?.proses_booking} />
            <Modal isVisible={showModal} data={selectedItem} onClose={() => setShowModal(false)} />

        </Layout>
        </>
    );
}

export default GetDNA;