import HomeBanner from "../components/homeBanner";
import Services from "../components/services";
import Layout from "../components/Layout";

export default function Home() {
  return (
    <>
      <Layout>
        <section className="relative">
          <HomeBanner />
        </section>

        <section className="py-14 container mx-auto max-w-8xl">
          <h3 className="font-bold text-center text-3xl py-5">Our Services</h3>
          <Services />
        </section>
      </Layout>
    </>
  );
}
