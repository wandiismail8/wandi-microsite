import React from 'react';
import Link from 'next/link';

function Breadcrumb(props) {
    return (
        <nav className="rounded-md w-full py-3 md:px-0 px-4">
            <ul className="list-reset flex">
                <li><Link href="/" className="color-primary hover:text-teal-700 duration-500">Home</Link></li>
                <li><span className="text-gray-500 mx-2">/</span></li>
                {props.title && (
                    <li className="">{props.title}</li>
                )}
            </ul>
        </nav>
    );
}

export default Breadcrumb;