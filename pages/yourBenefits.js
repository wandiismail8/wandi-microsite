import React, { useEffect, useState }  from 'react';
import Image from 'next/image';
import Breadcrumb from '../components/Breadcrumb';
import {SlArrowUp, SlArrowDown} from 'react-icons/sl'
import {BsCheckCircle} from 'react-icons/bs'
import Layout from '../components/Layout';

function YourBenefits() {
    // GET DATA
    const getContent = () => fetch('benefits.json').then((resp) => resp.json());
    
    const [content, setContent] = useState({})
    const onLoadContent = async () => {
      const data = await getContent()
      setContent(data.data)
    }
  
    useEffect(() => {
      onLoadContent()
    }, [])

    // Modal
    const [selectedItem, setSelectedItem] = useState(null)
    const handleSelectContent = (item) => {
        console.log('item:', item)
        setSelectedItem(item)
    }

    const handleSelectContent2 = (item) => {
        setSelectedItem(item)
    }

    // Accordion
    const [selected, setSelected] = useState(null)
    const [selectedBenefit, setSelectedBenefit] = useState(null)
    const toggle = (i) => {
        if (selected === i) {
            return setSelected(null)
        }
        setSelected(i)
    }

    const toggleBenefit = (i) => {
        if (selectedBenefit === i) {
            return setSelectedBenefit(null)
        }
        setSelectedBenefit(i)
    }

    return (
        <>

        <Layout title={content.page_title} >

            <div className="mx-auto container p-8">                    
                <section className="md:flex block items-center gap-12 md:px-0 px-4 mb-32">
                    <div className='md:w-1/2 w-full'>
                        <h3 className='xl:text-6xl lg:text-4xl text-2xl color-primary font-bold py-6'>{content.title}</h3>
                        <p className='md:text-2xl text-xl md:leading-10 leading-7 pb-6'>{content.short_descriptions}</p>
                        {content.banner_buttons?.map((item) => (
                            <button key={item.id} className="bg-primary xl:py-3 py-2 xl:px-6 px-4 text-white rounded-lg flex justify-center items-center gap-2 xl:text-base text-sm">{item.title}</button>
                        ))}
                    </div>
                    <div className='md:w-1/2 w-full md:pt-0 pt-7'>
                        {content.banner_image_path && <Image src={content.banner_image_path} width={8} height={7} layout="responsive" objectFit='cover' alt={content.title} />}
                       

                    </div>
                </section>

                <section className="md:flex block items-center gap-12 py-14 md:px-0 px-4">
                    <div className='md:w-1/2 w-full'>
                        <h3 className='text-4xl font-bold py-6'>{content.application?.title}</h3>
                        <p className='text-xl md:leading-10 leading-7 md:pb-8'>{content.application?.short_description}</p>
                        {content.application?.benefits.map((benefit, i) => (
                            <React.Fragment key={benefit.id}>
                                <div className='flex items-center justify-between cursor-pointer' onClick={() => {toggle(i); handleSelectContent(benefit)}}>
                                    <p className='text-bold text-2xl py-3 font-bold'>{benefit.title}</p>
                                    <span>{selected === i ? <SlArrowUp /> : <SlArrowDown />}</span>
                                </div>
                                <div className={selected === i ? 'visible' : 'hidden'}>
                                    <ul className='mb-3'>
                                        {selectedItem?.content.map((item) => (
                                            <li key={item.id} className="flex items-start gap-2 md:py-3 py-2">
                                                <div className='w-6 mr-1 mt-1'><BsCheckCircle className='color-primary text-2xl' /></div>
                                                <p className='color-primary md:text-xl text-lg'>{item.description}</p>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            </React.Fragment>
                        ))}
                    </div>
                    <div className='md:w-1/2 w-full md:pt-0 pt-8 md:block hidden'>
                        <div className='flex justify-center'>
                            {content.application?.banner_image_path && <Image src={content.application?.banner_image_path} width="327" height="650" alt={content.application?.title} />}
                        </div>
                    </div>
                </section>
            </div>








            
            <section className='w-full md:block py-4 px-4 bg-green-90'>
                <div className='mx-auto max-w-6xl container md:flex md:justify-between'>
                    <p className='text-white md:text-2xl text-xl py-4'>{content.highlight_1?.title}</p>
                    <button className='py-3 mb-4 font-semibold bg-primary rounded-lg text-white px-6'>{content.highlight_1?.button}</button>
                </div>
            </section>
            <div className="mx-auto container">
                <section className="md:flex block items-start gap-12 py-14 md:px-0 px-4">
                    <div className='md:w-1/2 w-full md:pt-0 pt-8 md:block hidden'>
                        <div className='flex justify-start'>
                            {content.getCompany?.banner_image_path && <Image src={content.getCompany?.banner_image_path} width="600" height="650" alt={content.getCompany?.title} />}
                        </div>
                    </div>
                    <div className='md:w-1/2 w-full'>
                        <h3 className='text-4xl font-bold py-6'>{content.getCompany?.title}</h3>
                        <p className='text-xl md:leading-10 leading-7 md:pb-8'>{content.getCompany?.short_description}</p>
                        {content.getCompany?.benefits.map((benefit, i) => (
                            <React.Fragment key={benefit.id}>
                                <div className='flex items-center justify-between cursor-pointer' onClick={() => {toggleBenefit(i); handleSelectContent2(benefit)}}>
                                    <p className='text-bold text-2xl py-3 font-bold'>{benefit.title}</p>
                                    <span>{selectedBenefit === i ? <SlArrowUp /> : <SlArrowDown />}</span>
                                </div>
                                <div className={selectedBenefit === i ? 'visible' : 'hidden'}>
                                    <ul className='mb-3'>
                                        {selectedItem?.content.map((item) => (
                                            <li key={item.id} className="flex items-start gap-2 md:py-3 py-2">
                                                <div className='w-6 mr-1 mt-1'><BsCheckCircle className='color-primary text-2xl' /></div>
                                                <p className='color-primary md:text-xl text-lg'>{item.description}</p>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            </React.Fragment>
                        ))}
                    </div>
                </section>
            </div>
            <section className='w-full md:block py-4 px-4 bg-green-90'>
                <div className='mx-auto max-w-6xl container md:flex md:justify-between'>
                    <p className='text-white md:text-2xl text-xl py-4'>{content.highlight_2?.title}</p>
                    <button className='py-3 mb-4 font-semibold bg-primary rounded-lg text-white px-6'>{content.highlight_2?.button}</button>
                </div>
            </section>

        </Layout>
      
        </>
    );
}

export default YourBenefits;