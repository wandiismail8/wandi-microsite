import Image from "next/image";
import { useState, useEffect } from "react";
import Layout from "../components/Layout";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import { Navigation } from "swiper";
import LiteYouTubeEmbed from "react-lite-youtube-embed";
import "react-lite-youtube-embed/dist/LiteYouTubeEmbed.css";



const WhatTheySay = () => {
  const getTestimonial = () => fetch('whatTheySay.json').then((resp) => resp.json())
  const [testimonials, SetTestimonials] = useState([])
  const onLoadTestimonial = async () => {
    const data = await getTestimonial()
    SetTestimonials(data)
  }

  useEffect(() => {
    onLoadTestimonial()
  }, [])


  console.log(testimonials)



  return (
    <>
      <Layout title={testimonials?.data?.page_title}>
    
        <section>
          <div className="w-full h-[100vh]">
            <div className="container-slider">
              <div className="testimonial slide">
                <div>
                  <h1 className=" text-3xl flex justify-center py-16 text-white font-semibold">
                    {testimonials?.data?.slider_title}
                  </h1>
                </div>
                {
                  testimonials && (  
                <Swiper
                navigation={true}
                modules={[Navigation]}
                className="mySwiper"
              >
                {testimonials?.data?.banner_testimonial.map(item => (
                <SwiperSlide key={item.id}>
                  <div className="flex justify-center">
                    <Image
                      src={item.image_path}
                      width="400"
                      height="400"
                      alt=""
                      className="image"
                    />
                  </div>
                  <div className="details">
                    <span className="name">{item.name}</span>
                    <span className="job">{item.title}</span>
                  </div>
                  <p>
                   {item.desc} 
                  </p>
                </SwiperSlide>
                ))}
              </Swiper>

                  )
                }
              </div>
            </div>
          </div>
        </section>

        <section className="py-20 container mx-auto max-w-8xl">
          <h3 className="font-bold text-center text-3xl gap-96">
            {testimonials?.data?.hearFromClients?.title}
          </h3>

          {testimonials && (

          <section className="w-full py-32 h-[65vh] container ">
            <div className="grid md:grid-cols-3 sm:grid-cols-2 grid-cols-1 md:flex block md:justify-start justify-center gap-4 sm:px-0 px-4">
               {testimonials?.data?.hearFromClients?.videos.map((item) => (
              <div key={item.id} className="card  border-solid border-2 border-gray-200 rounded-xl p-4 min-h-full">
                <LiteYouTubeEmbed
                  id={item.video_url}
                />
                <h4 className="font-bold py-3 text-open-sans">{item.video_title}</h4>
                <p className="font-thin text-open-sans text-gray-400">{item.video_description}</p>
              </div>
               ) )}
               
               </div>
          </section>
          )}
        </section>
      </Layout>
    </>
  );
};

export default WhatTheySay;

