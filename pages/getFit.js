import React, { useEffect, useState } from "react";
import Image from "next/image";
import ProcessBooking from "../components/ProcessBooking";
import BannerButtons from "../components/BannerButtons";
import LiteYouTubeEmbed from "react-lite-youtube-embed";
import "react-lite-youtube-embed/dist/LiteYouTubeEmbed.css";
import { BsWhatsapp } from "react-icons/bs";
import Layout from "../components/Layout";

function GetFit() {
  // GET get CARE DATA
  const getContent = () => fetch("getFit.json").then((resp) => resp.json());

  const [content, setContent] = useState([]);
  const onLoadContent = async () => {
    const data = await getContent();
    setContent(data.data);
  };
  const highlightImage1 = content.highlight_1?.background_image_path;
  const highlightImage2 = content.highlight_2?.background_image_path;

  useEffect(() => {
    onLoadContent();
  }, []);

  // TABS
  const [openTab, setOpenTab] = useState(content.activities?.videos[0].id);
  const [videoId, setVideoId] = useState(
    content.activities?.videos[0]?.video_url
  );
  const initialVideo = content.activities?.videos[0]?.video_url;

  const [isActive, setIsActive] = useState(null);
  const toggleIsActive = (item) => {
    setIsActive(item);
  };
  const handleSelectVideo = (item) => {
    // console.log('klik', item)
    setOpenTab(item.id);
    setVideoId(item.video_url);
    toggleIsActive(item);
  };

  return (
    <>
      <Layout title={content.page_title} breadcrumb={content.breadcrumb}  >
        <div className="mx-auto container">

          <section>
            <div className="relative mix-blend-overlay">
              <div className="background-overlay flex items-center">
                <BannerButtons content={content} />
              </div>
              {content.banner_image_path && (
                <Image
                  src={content.banner_image_path}
                  width="1512"
                  height="600"
                  alt={content.page_title}
                  className="homepage-banner"
                />
              )}
            </div>
          </section>
        </div>
        <section className="flex grid-cols-2 items-center justify-start md:mb-24 mb-10 md:pt-10 md:pb-5">
          {highlightImage1 && (
            <div className="w-1/2 md:block hidden">
              <Image
                src={highlightImage1}
                width={1}
                height={1}
                layout="responsive"
                objectFit="cover"
                alt="get Doctor"
                className="md:block hidden highlight-image w-2/4"
              />
            </div>
          )}
          <div className="max-w-2xl md:px-10 px-4 md:w-1/2 w-full">
            <h3 className="md:text-3xl text-xl md:font-semibold font-bold py-3">
              {content.highlight_1?.title}{" "}
              <span className="color-primary">Healty Meal Plan</span>
            </h3>
            <h4 className="md:text-2xl text-xl md:font-semibold font-bold py-2">
              Dalam Aplikasi Kami
            </h4>
            <p className="mb-6">{content.highlight_1?.description}</p>
            <a
              href="#"
              className="py-2 px-4 bg-primary rounded-md text-white hover:bg-teal-600 duration-700 flex justify-start items-center gap-3 w-fit"
            >
              <BsWhatsapp />
              Pesan Sekarang
            </a>
          </div>
        </section>

        <section className="flex grid-cols-2 items-center justify-between md:mb-24 mb-10 md:pt-10 md:pb-5">
          <div className="max-w-xl md:px-10 px-4">
            <h3 className="md:text-3xl text-xl md:font-semibold font-bold py-3">
              {content.highlight_2?.title}
            </h3>
            <p className="mb-6">{content.highlight_2?.description}</p>
            <a
              href="#"
              className="py-2 px-4 bg-primary rounded-md text-white hover:bg-teal-600 duration-700 flex justify-start items-center gap-3 w-fit"
            >
              <BsWhatsapp />
              Pesan Sekarang
            </a>
          </div>
          {highlightImage2 && (
            <div className="w-1/3 md:block hidden">
              <Image
                src={highlightImage2}
                width={16}
                height={9}
                layout="responsive"
                objectFit="cover"
                alt="get Doctor"
                className="md:block hidden"
              />
            </div>
          )}
        </section>

        <section className="md:mb-24 mb-10 md:pt-10 md:pb-5">
          <h3 className="md:text-3xl text-xl md:font-semibold font-bold py-3 text-center">
            {content.nutritionist?.title}
          </h3>
          <p className="text-center mb-6">
            {content.nutritionist?.description}
          </p>
          <div className="md:flex block md:grid-cols-4 md:max-w-6xl w-full mx-auto gap-5 md:px-0 px-3 justify-center">
            {content.nutritionist?.professionals.map((item) => (
              <div key={item.id} className="relative md:mb-0 mb-5">
                <div className="absolute w-full bottom-0 py-5 bg-gradient-to-t from-black rounded-b-xl">
                  <h4 className="text-white text-center text-bold text-2xl">
                    {item.title}
                  </h4>
                  <p className="text-white text-center">{item.person}</p>
                </div>
                <Image
                  width={2}
                  height={3}
                  alt={item.title}
                  layout="responsive"
                  objectFit="cover"
                  src={item.image_path}
                  className="rounded-xl w-full"
                />
              </div>
            ))}
          </div>
        </section>

        <section className="md:mb-24 mb-10 md:pt-10 md:pb-5">
          <h3 className="md:text-3xl text-xl md:font-semibold font-bold py-3 text-center">
            {content.activities?.title}
          </h3>
          <p className="text-center mb-6">{content.activities?.description}</p>
          <div className="md:flex md:grid-cols-2 md:max-w-6xl justify-between mx-auto w-full md:px-0 px-4">
            <div className="md:w-1/2 w-full md:px-4 px-0">
              <div className="bg-slate-300">
                <div className={openTab === openTab ? "block" : "hidden"}>
                  <div>
                    {videoId ? (
                      <LiteYouTubeEmbed id={videoId} />
                    ) : (
                      <LiteYouTubeEmbed id={initialVideo} />
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="md:w-1/2 w-full">
              {content.activities?.videos.map((video) => (
                <button
                  key={video.id}
                  className="md:mb-3 w-full text-left p-3"
                  onClick={() => {
                    handleSelectVideo(video);
                  }}
                >
                  <div
                    className={
                      isActive === video
                        ? "btn-active w-full bottom-0 border-solid border-2 border-primary rounded-xl p-3 flex gap-4 hover:bg-teal-100 duration-500"
                        : "w-full bottom-0 border-solid border-2 border-primary rounded-xl p-3 flex gap-4 hover:bg-teal-100 duration-500"
                    }
                  >
                    <div>
                      <Image
                        src={video.icon}
                        width="48"
                        height="48"
                        alt="icon"
                      />
                    </div>
                    <div>
                      <h4 className="text-bold text-xl color-primary">
                        {video.video_title}
                      </h4>
                      <p className="text-sm">{video.video_description}</p>
                    </div>
                  </div>
                </button>
              ))}
            </div>
          </div>
        </section>

        <ProcessBooking processBooking={content?.proses_booking} />
      </Layout>
    </>
  );
}

export default GetFit;
