import React, { useEffect, useState }  from 'react';
import Image from 'next/image';
import LiteYouTubeEmbed from "react-lite-youtube-embed";
import "react-lite-youtube-embed/dist/LiteYouTubeEmbed.css";
import ProcessBooking from '../components/ProcessBooking';
import BannerButtons from '../components/BannerButtons';
import Layout from '../components/Layout';

function GetCounselling() {
    // GET get MENTALLY DATA
    const getContent = () => fetch('getCounseling.json').then((resp) => resp.json());
    
    const [content, setContent] = useState([])
    const onLoadContent = async () => {
      const data = await getContent()
      setContent(data.data)
    }
    const highlightImage = content.highlight?.background_image_path
  
    useEffect(() => {
      onLoadContent()
    }, [])

    // TABS
    // const [openTab, setOpenTab] = useState(content.activities?.videos[0].id);
  const [videoId, setVideoId] = useState(
    content.activities?.videos[0]?.video_url
  );
  const initialVideo = content.activities?.videos[0]?.video_url;

  
  const handleSelectVideo = (item) => {
    // console.log('klik', item)
    setOpenTab(item.id);
    setVideoId(item.video_url);
    toggleIsActive(item);
  };
    const [openTab, setOpenTab] = useState(content.categories?.categories[0].id);
    const [description, setDescription] = useState(content.categories?.categories[0]?.description)
    const [title, setTitle] = useState(content.categories?.categories[0]?.name)
    const initialDescription = content.categories?.categories[0]?.description
    const initialTitle = content.categories?.categories[0]?.name
    const [selectedIndex, setSelectedIndex] = useState(0)
    
    const [isActive, setIsActive] = useState(null);
    const toggleIsActive = (item) => {
        setIsActive(item)
    }
    const handleSelectCategory = (item) => {
        setOpenTab(item.id)
        setTitle(item.name)
        setDescription(item.description)
        toggleIsActive(item)
        setSelectedIndex(null)
    }

    return (
        <>


        <Layout title={content.page_title} breadcrumb={content.breadcrumb} >


            <div className="mx-auto container">    
                
                <section>
                    <div className="relative mix-blend-overlay">
                        <div className="background-overlay flex items-center">
                            <BannerButtons content={content} />
                        </div>
                        {content.banner_image_path && (
                            <Image src={content.banner_image_path} width="1512" height="600" alt={content.page_title} className="homepage-banner"  />
                        )}
                    </div>
                </section>

                <section className="container mx-auto max-w-8xl py-14">
                    <h3 className="font-bold text-center text-xl pt-5 pb-2">{content.secondary_title}</h3>
                    <p className='text-center text-md pb-9'>{content.secondary_description}</p>
                    <div className="flex">
                        <div className='w-1/4 border-2 rounded-lg shadow-md'>
                            <h3 className='text-3xl text-center font-light py-4'>Kategori Psikolog</h3>
                            {/* isActive === item   block py-3 font-semibold text-xl */}

                            {/* <div style={{'backgroundColor': status === 'approved' ? 'blue' : status === 'pending' ? 'black' : 'red'}}></div> */}

                            <div className='ml-4 -mr-4'>
                            {content.categories?.categories.map((item, index) => (
                                <button key={item.id} className={
                                    selectedIndex === index  || isActive === item ? 'block w-full  py-3 font-semibold text-xl bg-teal-100 text-left text-teal-700 -ml-4 border-l-4 border-teal-500 px-6'  : 'block py-3 font-semibold text-xl'
                                } onClick={() => { handleSelectCategory(item) }}>{item.name}</button>
                            ))}

                            </div>

                        </div>
                        <div className='w-3/4 border-2 ml-[38px] rounded-lg shadow-md'>
                            <div className={openTab === openTab ? "block" : "hidden"}>
                                <h4 className='text-4xl font-semibold ml-9 mb-4 color-primary  pt-9'>{title ? title : initialTitle}</h4>
                                <p className='text-xl ml-9'>{description ? description : initialDescription}</p>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="container mx-auto max-w-8xl py-14">
                
                    <h3 className="font-bold text-center text-xl pt-5 pb-4">{content.psikolog_title}</h3>
                    <div className="grid xl:grid-cols-3 sm:grid-cols-2 grid-cols-1 gap-4 sm:px-0 px-4">
                        {content.psychologs?.map((psycholog) => (
                            <div key={psycholog.id} className="card shadow-md border-solid border-2 border-gray-200 rounded-xl p-4 flex">
                                <div>
                                    <Image src={psycholog.image_path} width="120" height="48" alt={psycholog.title} />
                                </div>
                                <div>
                                    <h4 className="font-bold py-3 text-open-sans">{psycholog.name}</h4>
                                    <h4 className="pb-2 text-open-sans text-gray-600">{psycholog.title}</h4>
                                    <div className="box-border border border-gray-200 rounded-md py-1 px-2 font-thin text-open-sans w-fit">STR: {psycholog.str}</div>
                                </div>
                            </div>
                        ))}
                    </div>
                </section>
            </div>


            <section className="md:mb-24 mb-10 md:pt-10 md:pb-5">
          <h3 className="md:text-3xl text-xl md:font-semibold font-bold py-3 text-center">
            {content.activities?.title}
          </h3>
          <p className="text-center mb-6">{content.activities?.description}</p>
          <div className="md:flex md:grid-cols-2 md:max-w-6xl justify-between mx-auto w-full md:px-0 px-4">
            <div className="md:w-1/2 w-full md:px-4 px-0">
              <div className="bg-slate-300">
                <div className={openTab === openTab ? "block" : "hidden"}>
                  <div>
                    {videoId ? (
                      <LiteYouTubeEmbed id={videoId} />
                    ) : (
                      <LiteYouTubeEmbed id={initialVideo} />
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="md:w-1/2 w-full">
              {content.activities?.videos.map((video) => (
                <button
                  key={video.id}
                  className="md:mb-3 w-full text-left p-3"
                  onClick={() => {
                    handleSelectVideo(video);
                  }}
                >
                  <div
                    className={
                      isActive === video
                        ? "btn-active w-full bottom-0 border-solid border-2 border-primary rounded-xl p-3 flex gap-4 hover:bg-teal-100 duration-500"
                        : "w-full bottom-0 border-solid border-2 border-primary rounded-xl p-3 flex gap-4 hover:bg-teal-100 duration-500"
                    }
                  >
                    <div>
                      <Image
                        src={video.icon}
                        width="48"
                        height="48"
                        alt="icon"
                      />
                    </div>
                    <div>
                      <h4 className="text-bold text-xl color-primary">
                        {video.video_title}
                      </h4>
                      <p className="text-sm">{video.video_description}</p>
                    </div>
                  </div>
                </button>
              ))}
            </div>
          </div>
        </section>
            
            <section className='flex items-center justify-start md:mb-0 mb-10'>
                {highlightImage && (
                    <Image src={highlightImage} width="500" height="800" alt='get Doctor' className='md:block hidden' />
                )}
                <div className='flex justify-center w-full'>
                    <div className='max-w-2xl px-4'>
                        <h3 className='md:text-3xl text-xl md:font-semibold font-bold py-3'>{content.highlight?.title}</h3>
                        <p className='mb-6 md:text-xl'>{content.highlight?.description}</p>
                        <a href='#' className='py-2 px-4 bg-primary rounded-md text-white hover:bg-teal-600 duration-700'>Install Aplikasi</a>
                    </div>
                </div>
            </section>



       


            <ProcessBooking processBooking={content?.proses_booking} />

        </Layout>



        </>
    );
}

export default GetCounselling;