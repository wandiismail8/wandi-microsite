import React, { useEffect, useState }  from 'react';
import Image from 'next/image';
import Breadcrumb from '../components/Breadcrumb';
import ProcessBooking from '../components/ProcessBooking';
import BannerButtons from '../components/BannerButtons';
import Layout from '../components/Layout';

function GetLab() {
    // GET get LAB DATA
    const getContent = () => fetch('getLab.json').then((resp) => resp.json());
    
    const [content, setContent] = useState([])
    const onLoadContent = async () => {
      const data = await getContent()
      setContent(data.data)
    }
    const highlightImage1 = content.highlight_1?.background_image_path
    const highlightImage2 = content.highlight_2?.background_image_path
  
    useEffect(() => {
      onLoadContent()
      console.log('load')
    }, [])

    return (
        <>
            <Layout title={content.page_title } breadcrumb={content.breadcrumb} >
                
                <section>
                    <div className="relative mix-blend-overlay">
                        <div className="background-overlay flex items-center">
                            <BannerButtons content={content} />
                        </div>
                        {content.banner_image_path && (
                            <Image src={content.banner_image_path} width="1512" height="600" alt={content.page_title} className="homepage-banner"  />
                        )}
                    </div>
                </section>

                <section className="py-14 p-10">
                    <h3 className="font-bold text-center text-xl pt-5 pb-2">{content.secondary_title}</h3>
                    <p className='text-center text-md pb-3'>{content.secondary_description}</p>
                    <div className="grid xl:grid-cols-4 sm:grid-cols-2 grid-cols-1 gap-4 sm:px-0 px-4">
                        {content.labs?.map((service) => (
                            <div key={service.id} className="card shadow-md border-solid border-2 border-gray-200 rounded-xl p-4">
                                <div>
                                    <Image src={service.image_path} width="60" height="60" alt={service.title} />
                                </div>
                                <div>
                                    <h4 className="font-bold py-3 text-open-sans">{service.title}</h4>
                                    <h4 className="pb-2 text-open-sans text-gray-600">{service.description}</h4>
                                </div>
                            </div>
                        ))}
                    </div>
                </section>

                <section className='flex grid-cols-2 items-center justify-evenly md:mb-24 mb-10 md:pt-10 md:pb-5'>
                    {highlightImage1 && (
                        <Image src={highlightImage1} width={16} height={9} layout="responsive" objectFit='cover' alt='get Doctor' className='md:block hidden highlight-image' />
                    )}
                    <div className='max-w-xl md:px-10 px-4'>
                        <h3 className='md:text-3xl text-xl md:font-semibold font-bold py-3'>{content.highlight_1?.title}</h3>
                        <p className='mb-6'>{content.highlight_1?.description}</p>
                        <a href='#' className='py-2 px-4 bg-primary rounded-md text-white hover:bg-teal-600 duration-700'>Install Aplikasi</a>
                    </div>
                </section>

                <section className='flex grid-cols-2 items-center justify-evenly md:mb-24 mb-10 md:pt-10 md:pb-5'>
                    <div className='max-w-xl md:px-10 px-4'>
                        <h3 className='md:text-3xl text-xl md:font-semibold font-bold py-3'>{content.highlight_2?.title}</h3>
                        <p className='mb-6'>{content.highlight_2?.description}</p>
                        <a href='#' className='py-2 px-4 bg-primary rounded-md text-white hover:bg-teal-600 duration-700'>Install Aplikasi</a>
                    </div>
                    {highlightImage2 && (
                        <Image src={highlightImage2} width={16} height={9} layout="responsive" objectFit='cover' alt='get Doctor' className='md:block hidden' />
                    )}
                </section>

            <ProcessBooking processBooking={content?.proses_booking} />     
            </Layout>
        </>
    );
}

export default GetLab;