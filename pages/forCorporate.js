import Layout from "../components/Layout";
import React from "react";
import Image from "next/image";
import ForCorporateBanner from "../components/ForCorporateBanner";
import { useEffect, useState } from "react";

const forCorporate = () => {
  // const getContent = () => fetch('forCorporate.json').then((resp) => resp.json());

  //   const [content, setContent] = useState({})
  //   const onLoadContent = async () => {
  //     const data = await getContent()
  //     setContent(data.data)
  //   }

  //   useEffect(() => {
  //     onLoadContent()
  //   }, [])

  return (
    <>
      <Layout title={"For Corporate"}>
        <section className="container mx-auto">
          <ForCorporateBanner />
        </section>

   
        <section className="h-[80vh] w-full">
          <div className=" flex-col">
            <div className="container m-auto justify-items-center text-center text-[64px] max-w-8xl ">
              <p className=" text-[#3BACB6] mx-48 my-20">
                <span className="font-bold">Empowering</span> your colleagues.
                Rise your <span className="font-bold">team spirit.</span>
              </p>
            </div>
            <div className="container m-auto justify-between text-center text-[64px] max-w-8xl">
              <p className="text-[#3BACB6] ">
                Increase your company productivity in a{" "}
                <span className="font-bold">healthy way ...</span>
              </p>
            </div>
          </div>
        </section>



        <section className="h-[912px] w-full mx-auto">
    
            <Image className="container mx-auto"
            src={require("../public/img/corporatePackage/percentageData.png")}
            alt=""  
            width={1536}
            height={746} />

        </section>


        <section className="h-[912px] w-full mx-auto">
    
            <Image className="container mx-auto"
            src={require("../public/img/corporatePackage/corporateBenefits.png")}
            alt=""  
            width={1536}
            height={746} />

        </section>
        <div className="mt-2">
          <section>
            <div className="flex justify-between text-center py-28 flex-col w-full h-screen gap-10 bg-[#FFFDFD]">
              <div className="">
                <h1 className="text-7xl max-w-7xl mx-auto container text-left text-black mt-10">
                  Corporate Package
                </h1>
              </div>
              <div>
                <h3 className="color-primary text-left container mx-auto max-w-7xl mt-4 mb-4 font-semibold text-2xl">
                  Our package to suit your corporate healthy lifestyle
                </h3>
              </div>
              <div className="text-3xl text-gray-500 mx-auto leading-relaxed text-left container mb-48 max-w-7xl">
                <p>
                  Corporate package adalah paket bundling dari program gabungan
                  yang dimiliki getwell dan merupakan andalan bagi perusahaan
                  yang ingin meningkatkan kualitas kesehatan para karyawannya
                  untuk mencapai produktivitas yang optimal.
                </p>
              </div>
            </div>
          </section>

          <div className="container max-w-8xl mx-auto">
            <section className="md:flex block h-[82vh] items-center gap-20 md:px-0 px-4 mb-32">
              <div className="md:w-1/2 w-full md:pt-0 pt-7">
                {/* {content.banner_image_path && <Image src={content.banner_image_path} width={8} height={7} layout="responsive" objectFit='cover' alt={content.title} />} */}
                <Image
                  src={require("../public/img/corporatePackage/pastiBisa.png")}
                  width={10}
                  height={9}
                  layout="responsive"
                  objectFit="cover"
                  alt=""
                />
              </div>
              <div className="md:w-1/2 w-full">
                <h3 className="xl:text-6xl lg:text-4xl text-2xl color-primary font-bold py-6">
                  {" "}
                  <span className="border-primary absolute w-20 h-20 rounded-full border-2 flex justify-center items-center -ml-24  color-primary ">
                    1
                  </span>
                  Pasti Bisa
                </h3>
                <p className="md:text-2xl text-xl text-gray-500 md:leading-10 leading-7 pb-6">
                  Pilihan sempurna bagi perusahaan untuk memberikan layanan
                  kesehatan yang lebih lengkap
                </p>
                <div className="color-primary -mt-4">
                  <p>#yeswecan</p>
                </div>
              </div>
            </section>

            <section className="md:flex block items-center gap-12 md:px-0 px-4 mb-32">
              <div className="md:w-1/2 w-full">
                <h3 className="xl:text-6xl lg:text-4xl text-2xl ml-16 color-orange font-bold py-6">
                  <span className="border-[orange] absolute w-20 h-20 rounded-full border-2 flex justify-center items-center -ml-24  color-orange">
                    2
                  </span>
                  Semangat
                </h3>
                <p className="md:text-2xl text-xl ml-16 md:leading-10 leading-7 pb-6">
                  Sangat cocok diberikan kepada perusahaan yang ingin lebih
                  memberikan jaminan kesehatan lebih baik kepada para
                  karyawannya, seperti program makanan nutrisi
                </p>
                <div className="color-orange ml-16 -mt-4">
                  <p>#energize</p>
                </div>
              </div>
              <div className="md:w-1/2 w-full md:pt-0 pt-7">
                <Image
                  src={require("../public/img/corporatePackage/semangat.png")}
                  width={8}
                  height={7}
                  layout="responsive"
                  objectFit="cover"
                  alt=""
                />
              </div>
            </section>

            <section className="md:flex block h-[82vh] items-center gap-20 md:px-0 px-4 mb-32">
              <div className="md:w-1/2 w-full md:pt-0 pt-7">
                {/* {content.banner_image_path && <Image src={content.banner_image_path} width={8} height={7} layout="responsive" objectFit='cover' alt={content.title} />} */}
                <Image
                  src={require("../public/img/corporatePackage/hebat.png")}
                  width={10}
                  height={9}
                  layout="responsive"
                  objectFit="cover"
                  alt=""
                />
              </div>
              <div className="md:w-1/2 w-full">
                <h3 className="xl:text-6xl lg:text-4xl text-2xl color-red font-bold py-6">
                  {" "}
                  <span className="border-[red] absolute w-20 h-20 rounded-full border-2 flex justify-center items-center -ml-24  color-red">
                    3
                  </span>
                  Hebat
                </h3>
                <p className="md:text-2xl text-xl text-gray-500 md:leading-10 leading-7 pb-6">
                  Pilihan sempurna bagi perusahaan untuk memberikan layanan
                  kesehatan yang lebih lengkap
                </p>
                <div className="color-red -mt-4">
                  <p>#extraordinary</p>
                </div>
              </div>x
            </section>
          </div>

          <section className="w-full md:block h-screen py-4 px-4 bg-green-90">
            <div className="mx-auto flex flex-col text-center max-w-6xl md:flex  container">
              <div className="md:py-64 py-40 md:flex md:justify-center flex-col ">
                <p className="text-white text   md:text-4xl lg:text-5xl text-3xl mb-20  ">
                  Set your employees goals to meet your company goals,
                  <span> GET START NOW !</span>
                </p>
                <div className="">
                  <button className="py-3 mb-4 font-semibold bg-primary rounded-lg text-white px-6">
                    get in touch
                  </button>
                </div>
              </div>
            </div>
          </section>
        </div>
      </Layout>
    </>
  );
};

export default forCorporate;
