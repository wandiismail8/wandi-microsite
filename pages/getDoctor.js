import React, { useEffect, useState } from "react";
import Image from "next/image";
import ProcessBooking from "../components/ProcessBooking";
import BannerButtons from "../components/BannerButtons";
import Layout from "../components/Layout";

function GetDoctor() {
  // GET get DOCTOR DATA
  const getContent = () => fetch("getDoctor.json").then((resp) => resp.json());

  const [content, setContent] = useState({});
  const onLoadContent = async () => {
    const data = await getContent();
    setContent(data.data);
  };
  const highlightImage = content.highlight?.background_image_path;

  useEffect(() => {
    onLoadContent();
  }, []);

  return (
    <>
      <Layout title={content.page_title} breadcrumb={content.breadcrumb}>
        <div className="">
          <section>
            <div className="relative mix-blend-overlay  bg-slate-100">
              <div className="background-overlay   background-overlay flex items-center">
                <BannerButtons content={content} />
              </div>
              {content.banner_image_path && (
                <Image
                  src={content.banner_image_path}
                  width="1512"
                  height="600"
                  alt={content.page_title}
                  className="homepage-banner"
                />
              )}
            </div>
          </section>

          <section className="py-14 p-8 bg-slate-100 ">
            <h3 className="font-bold text-center text-xl pt-5 pb-2">
              {content.secondary_title}
            </h3>
            <p className="text-center text-md pb-3">
              {content.secondary_description}
            </p>
            <div className="grid xl:grid-cols-3 bg-white container mx-auto max-w-8xl sm:grid-cols-2 grid-cols-1 gap-4 sm:px-0 px-4 ">
              {content.doctors?.map((doctor) => (
                <div
                  key={doctor.id}
                  className="card shadow-md relative border-solid h-48 border-2 border-gray-200 rounded-xl p-4 flex gap-4"
                >
                  <div className="bg-slate-300 w-24 h-24   rounded-br-3xl rounded-tl-3xl rounded-bl-md rounded-tr-md " />

                  <div className="absolute bottom-4">
                    <Image
                      src={doctor.image_path}
                      width="120"
                      height="48"
                      alt={doctor.title}
                    />
                  </div>

                  <div>
                    <h4 className="font-bold py-3 text-open-sans">
                      {doctor.name}
                    </h4>
                    <h4 className="pb-2 text-open-sans text-gray-600">
                      {doctor.title}
                    </h4>
                    <div className="box-border text-sm border border-gray-200 rounded-md py-1 px-2 font-thin text-open-sans w-fit">
                      STR: {doctor.str}
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </section>
        </div>

        <section className="flex items-center justify-start md:mb-0 mb-10">
          {highlightImage && (
            <Image
              src={highlightImage}
              width="500"
              height="800"
              alt="get Doctor"
              className="md:block hidden"
            />
          )}
          <div className="flex justify-center w-full">
            <div className="max-w-2xl px-4">
              <h3 className="md:text-3xl text-xl md:font-semibold font-bold py-3">
                {content.highlight?.title}
              </h3>
              <p className="mb-6 md:text-xl">
                {content.highlight?.description}
              </p>
              <a
                href="#"
                className="py-2 px-4 bg-primary rounded-md text-white hover:bg-teal-600 duration-700"
              >
                Install Aplikasi
              </a>
            </div>
          </div>
        </section>

        <ProcessBooking processBooking={content?.proses_booking} />
      </Layout>
    </>
  );
}

export default GetDoctor;
