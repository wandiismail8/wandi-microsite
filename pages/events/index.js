import React, { useEffect, useState }  from 'react';
import Image from 'next/image';
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import Breadcrumb from '../../components/Breadcrumb';
import Link from 'next/link';
import Head from 'next/head';

function GetDoctor() {
    // SLIDER SETTINGS
    const settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        autoplay: false
    }

    // GET get DOCTOR DATA
    const getContent = () => fetch('listEvents.json').then((resp) => resp.json());
    
    const [content, setContent] = useState({})
    const onLoadContent = async () => {
      const data = await getContent()
      setContent(data)
    }
  
    useEffect(() => {
      onLoadContent()
    }, [])

    return (
        <>
            <Head>
                <title>Getwell {content.page_title}</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <div className="mx-auto container">    
                <Navbar />
                {console.log('content', content)}
                <Breadcrumb title={content.data?.page_title} />
                
                <section className='md:py-14 py-6 xl:px-0 px-4'>
                    <h3 className="font-bold text-3xl pt-5 pb-2 text-center">{content.data?.subtitle}</h3>
                    <p className='text-md pb-3 md:text-xl text-lg w-full text-center mb-12'>{content.data?.subtitle_description}</p>
                    {content.events?.map((item) => (
                        <div key={item.id} className='w-full md:flex block justify-start items-start gap-9 mb-10'>
                            <div className='md:w-1/3 w-full'>
                                <Image src={item.image_path} alt={item.name} width={2} height={1} layout="responsive" objectFit='cover' className='rounded-2xl' />
                            </div>
                            <div className='md:w-2/3 w-full'>
                                <p className='text-base text-gray-700 pb-3'>{item.time}</p>
                                <p className='text-2xl font-bold pb-3'>
                                    <Link href={'/events/' + item.id}>{item.title}</Link>
                                </p>
                                <p className='text-base pb-3'>{item.description}</p>
                                <button className='rounded-xl text-white px-6 py-3 text-semibold bg-primary my-4'>Kontak Kami</button>
                            </div>
                        </div>
                    ))}
                </section>                
            </div>
            
            <Footer />
        </>
    );
}

export default GetDoctor;