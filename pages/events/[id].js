import React, { useEffect, useState }  from 'react';
import Image from 'next/image';
import PropTypes from 'prop-types'
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import Breadcrumb from '../../components/Breadcrumb';
import Link from 'next/link';
import Head from 'next/head';

function Events() {
    // GET DATA
    const getContent = () => fetch('listEvents.json').then((resp) => resp.json());
    
    const [content, setContent] = useState({})
    const onLoadContent = async () => {
      const data = await getContent()
      setContent(data)
    }
  
    useEffect(() => {
      onLoadContent()
    }, [])

    return (
        <>
            <Head>
                <title>Getwell {content.page_title}</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <div className="mx-auto container">    
                <Navbar />
                <Breadcrumb title={content.page_title} />
                
                {console.log('content 2:', content)}
                <section className='md:py-14 py-6 xl:px-0 px-4'>
                    <h3 className="font-bold text-3xl pt-5 pb-2 text-center">test</h3>
                    <p className='text-md pb-3 md:text-xl text-lg w-full text-center mb-12'>test</p>
                </section>
            </div>
            
            <Footer />
        </>
    );
}

export default Events;