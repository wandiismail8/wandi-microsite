import { useEffect, useState } from 'react'
import Image from 'next/image'
import Link from 'next/link';

export default function Services() {
    // GET SERVICES DATA
    const gerServices = () => fetch('ourServices.json').then((resp) => resp.json());
    
    const [services, setServices] = useState([])
    const onLoadServices = async () => {
      const data = await gerServices()
      setServices(data)
    }
  
    useEffect(() => {
      onLoadServices()
    }, [])

    return (
        <>
            {services && (
                <div className="grid xl:grid-cols-4 md:grid-cols-3 grid-cols-1 gap-4 sm:px-0 px-4">
                    {services?.map((service) => (
                        <Link href={service.link} key={service.id}>
                            <div className="card border-solid border-2 border-gray-200 rounded-xl p-4 min-h-full">
                                <Image src={service.image_path} width="48" height="48" alt={service.title} />
                                <h4 className="font-bold py-3 text-open-sans">{service.title}</h4>
                                <p className="font-thin text-open-sans text-gray-400">{service.description}</p>
                            </div>
                        </Link>
                    ))}
                </div>
            )}
        </>
    )
}
