import React from 'react';
import Image from 'next/image';
import { HiOutlineMail } from 'react-icons/hi';
import { BsPhone, BsYoutube, BsInstagram } from 'react-icons/bs';
import {FaLinkedinIn} from 'react-icons/fa'
// assets
import appStore from '/public/img/appStore.png'
import playStore from '/public/img/playStore.png'

function Footer() {
    return (
        <>
        <div>
            <footer className='pt-10 w-full bg-primary  md:py-6 py-4 md:px-0 px-3'>
                <div className='mx-auto container max-w-8xl md:flex block md:justify-between my-3'>
                    <div>
                        <div className='py-3'>
                            <p className='text-white opacity-60'>Kantor Pusat</p>
                            <p className='text-white font-bold'>PT. Telemedika Teknologi Indonesia</p>
                            <p className='text-white font-thin'>Jl. Arco raya no. 77 Cipete, Jakarta Selatan</p>
                        </div>

                        <div className='py-3'>
                            <p className='text-white opacity-60'>Kontak Kami</p>
                            <p className='text-white font-bold'><HiOutlineMail className='inline-flex mr-2' /> <a href='mailto:administrasi@getwell.co.id'>administrasi@getwell.co.id</a></p>
                            <p className='text-white font-thin'><BsPhone className='inline-flex mr-2' /> 021 2932 5388</p>
                        </div>
                    </div>
                    <div>
                        <p className='text-white opacity-60'>Download Aplikasi Getwell</p>
                        <div className='flex gap-3 py-3'>
                            <a href='#'>
                                <Image src={appStore} width="240" height="68" alt="App Store" />
                            </a>
                            <a href='#'>
                                <Image src={playStore} width="240" height="68" alt="Play Store" />
                            </a>
                        </div>
                    </div>
                </div>
            </footer>

        </div>
            <div className='w-full bg-black md:py-4 py-3 px-3 md:text-left text-center font-normal text-sm'>
                <div className='mx-auto max-w-8xl container md:flex md:justify-between'>
                    <p className='color-primary'>&copy; 2022 Getwell Application | <a href='#'>Privacy Policy</a> | <a href='#'>Terms and Conditions</a></p>
                    <div className='flex md:items-center justify-center gap-3 md:py-0 py-2'>
                        <p className='color-primary'>Ikuti Kami</p>
                        <a href='#' className='bg-primary p-1 rounded-sm'><FaLinkedinIn /></a>
                        <a href='#' className='bg-primary p-1 rounded-sm'><BsYoutube /></a>
                        <a href='#' className='bg-primary p-1 rounded-sm'><BsInstagram /></a>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Footer;