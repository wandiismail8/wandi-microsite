import Image from 'next/image'
import { useEffect, useState } from 'react'
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import 'swiper/css/effect-fade';
import { Pagination, Autoplay, EffectFade } from "swiper";

export default function HomeBanner() {
    // SLIDER SETTINGS
        const settings = {
        dots: true,
        infinite: false,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        autoplay: true
    }
    
      // GET BANNERS DATA
      const getBanner = () => fetch('homebanner.json').then((resp) => resp.json());
    
      const [banners, setBanners] = useState([])
      const onLoadBanner = async () => {
        const data = await getBanner()
        setBanners(data)
      }
    
      useEffect(() => {
        onLoadBanner()
      }, [])
    
      return (
        <>
            {banners && (
                <Swiper
                    pagination={{
                        dynamicBullets: true,
                    }}
                    modules={[Pagination, Autoplay, EffectFade] } effect="fade"
                    className="mySwiper"
                    autoplay={{delay : 5000}}
                >
                    {banners?.map(item => (
                        <SwiperSlide key={item.id}>
                            <div className="relative mix-blend-overlay">
                                <div className="background-overlay">
                                    <h2 className="xl:text-7xl md:text-4xl text-2xl text-white absolute xl:bottom-24 md:bottom-12 bottom-16 xl:pl-10 pl-4 font-bold">{item.title}</h2>
                                    <p className="xl:text-2xl text-md text-white absolute xl:bottom-12 bottom-5 xl:pl-10 pl-4 md:pr-4 pr-6 font-thin text-open-sans">{item.description}</p>
                                </div>
                                <Image className="homepage-banner" src={item.image_path} width="1512" height="600" alt={item.title} />
                            </div>
                        </SwiperSlide>
                    ))}
                </Swiper>
            )}
        </>
    )
}