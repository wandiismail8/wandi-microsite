import React from 'react';
import {TfiClose} from 'react-icons/tfi'
import Image from 'next/image';

const Modal = ({ isVisible, onClose, data }) =>  {
    if ( !isVisible ) return null
    return (
        <div className='fixed inset-0 bg-black bg-opacity-30 flex justify-center items-center duration-500 overflow-scroll'>
            <div className='xl:w-1/2 w-full'>
                <div className='bg-white p-8 rounded-lg'>
                    <div className='flex justify-between xl:pt-0 pt-8'>
                        <h4 className='font-bold text-2xl'>Deskripsi</h4>
                        <button onClick={() => onClose()}><TfiClose /></button>
                    </div>
                    <div className='py-5'>
                        <Image className="rounded-lg" src={data.image_path} width="120" height="120" alt={data.title} />
                        <h4 className='font-bold text-2xl py-3'>{data.title} <span className='superscript'>{data.superscript}</span></h4>
                        <p className='py-3'>{data.paragraph_1}</p>
                        <p className='py-3'>{data.paragraph_2}</p>
                    </div>
                    <div className='xl:flex block grid-cols-2 justify-between items-center'>
                        <p className='xl:text-xl text-md'>{data.text_footer}</p>
                        <button className='bg-primary rounded-lg py-2 px-6 text-white xl:my-0 my-2'>{data.button_text_2}</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Modal;