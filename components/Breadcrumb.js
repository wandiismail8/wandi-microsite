import React from 'react';
import Link from 'next/link';

function Breadcrumb(props) {
    return (
        <nav className="w-full py-2 bg-white md:px-0 px-4">
            <div className='mx-auto max-w-8xl container'>
            <ul className="list-reset flex">
                <li><Link href="/" className="text-gray-500 hover:text-teal-700 duration-500">Home</Link></li>
                <li><span className="text-gray-500 mx-2">/</span></li>
                {props.title && (
                    <li className="color-primary">{props.title}</li>
                )}
            </ul>

            </div>
        </nav>
    );
}

export default Breadcrumb;