import React, { useState } from "react";
import { useRouter } from "next/router";
import Image from "next/image";
import Link from "next/link";
// assets
import logo from "/public/img/logo.png";
import Breadcrumb from "./Breadcrumb";

function Navbar(props) {
  const router = useRouter();

  const Links = [
    { id: "2", name: "For Corporate", link: "forCorporate", slug: "Corporate" },
    {
      id: "3",
      name: "Your Benefit",
      link: "yourBenefits",
      slug: "Your Benetifs",
    },
    {
      id: "4",
      name: "What They Say",
      link: "WhatTheySay",
      slug: "Testimonials",
    },
  ];

  const [open, setOpen] = useState(false);

  return (
    <div className="w-full bg-white md:py-2 px-2 md:text-left text-center font-normal text-sm">
      <div className="container max-w-8xl mx-auto ">
        <div className="md:flex bg-white my-1 md:px-4 px-7 justify-between">
          <div className="py-2">
            <Link href={"/"}>
              <Image src={logo} alt="logo" width="100" />
            </Link>
          </div>
          <div
            className="text-3xl absolute right-8 top-2 cursor-pointer md:hidden"
            onClick={() => setOpen(!open)}
          >
            <button className="mobile-menu-button">
              <svg
                className="w-6 h-6"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 6h16M4 12h16M4 18h16"
                />
              </svg>
            </button>
          </div>
          <ul
            className={`md:flex sm:block md:items-center md:pb-0 pb-6 absolute md:static bg-white md:z-auto z-[2] left-0 w-full md:w-auto md:pl-0 pl-9 ${
              open
                ? "top-20 opacity-100"
                : "top-[-490px] md:opacity-100 opacity-0"
            }`}
          >
            {Links.map((item) => (
              <li key={item.id} className="md:ml-8 md:my-0 my-7">
                <Link
                  className={
                    router.pathname === "/" + item.link
                      ? "duration-500 md:ml-8 md:gap-5 font-bold text-teal-400"
                      : "text-gray-800 hover:text-teal-400 duration-500 md:ml-8 md:gap-5 font-bold"
                  }
                  href={item.link}
                >
                  {item.name}
                </Link>
              </li>
            ))}
          </ul>
        </div>

        {props?.breadcrumbTitle && (
          <Breadcrumb title={props?.breadcrumbTitle} />
        )}
      </div>
    </div>
  );
}

export default Navbar;




