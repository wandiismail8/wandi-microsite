import Image from "next/image";
import { useState, useEffect } from "react";
import Layout from "../components/Layout";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import { Navigation, Pagination } from "swiper";
import LiteYouTubeEmbed from "react-lite-youtube-embed";
import "react-lite-youtube-embed/dist/LiteYouTubeEmbed.css";

const ForCorporateBanner = () => {
  const getTestimonial = () =>
    fetch("whatTheySay.json").then((resp) => resp.json());
  const [testimonials, SetTestimonials] = useState([]);
  const onLoadTestimonial = async () => {
    const data = await getTestimonial();
    SetTestimonials(data);
  };

  useEffect(() => {
    onLoadTestimonial();
  }, []);

  return (
    <>
   
        <section>
          <div className="w-full h-[100vh]">
            <div className="container-corporate">
              <div className="swiper-slide  slide">
                <div>
                  <h1 className=" text-3xl flex justify-center py-16 text-white font-semibold">
                    {testimonials?.data?.slider_title}
                  </h1>
                </div>
                {testimonials && (
                  <Swiper
                  slidesPerView={"auto"}
                  centeredSlides={true}
                  spaceBetween={30}
                  pagination={{
                    clickable: true,
                  }}
                  modules={[Pagination]}
                  className="mySwiper"
                  >
                    {testimonials?.data?.banner_testimonial.map((item) => (
                      <SwiperSlide key={item.id}>
                        <div className="flex justify-center">
                          <Image
                            src={item.image_path}
                            width="800"
                            height="500"
                            alt=""
                            className="img"
                          />
                        </div>
                        {/* <div className="details">
                          <span className="name">{item.name}</span>
                          <span className="job">{item.title}</span>
                        </div>
                        <p>{item.desc}</p> */}

                      </SwiperSlide>
                    ))}
                  </Swiper>
                )}
              </div>
            </div>
          </div>
        </section>
    </>
  );
};

export default ForCorporateBanner;
