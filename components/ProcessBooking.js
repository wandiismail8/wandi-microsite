import React from 'react';
import { BsArrowRightCircle, BsArrowDownCircle } from 'react-icons/bs';
import Image from 'next/image';

function ProcessBooking(props) {
    const process = props.processBooking
    return (
        <section className='bg-green-90 py-6'>
                <div className='mx-auto max-w-8xl container'>
                    <h3 className='font-bold text-center text-2xl py-5 text-white'>Proses Booking</h3>
                    <div className='grid md:grid-cols-5 gap-4 sm:px-0 px-3 process-booking-wrapper'>
                        {process?.map((item) => (
                            <div key={item.id} className="md:flex block md:justify-start justify-center">
                                <div className="md:text-left text-center item-booking-process">
                                    <Image src={item.image_path} width="90" height="90" alt={item.title} className="md:mx-0 mx-auto" />
                                    <p className='text-white text-xl font-semibold py-2'>{item.title}</p>
                                    <p className='text-white md:py-2 py-0 md:mb-0 mb-3 font-thin md:px-0 px-6'>{item.description}</p>
                                </div>
                                <div className='px-3 icon-flow md:flex hidden md:items-center'><BsArrowRightCircle className='text-4xl text-white'/></div>
                                <div className='px-3 icon-flow md:hidden flex justify-center'><BsArrowDownCircle className='text-4xl text-white'/></div>
                            </div>
                        ))}
                    </div>
                </div>
        </section>
    );
}

export default ProcessBooking;