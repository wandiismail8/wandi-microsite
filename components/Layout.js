import Head from "next/head";
import React from "react";
import Footer from "./Footer";
import Navbar from "./Navbar";
import {useRef} from "react"
// import TawkMessengerReact from "@tawk.to/tawk-messenger-react";

export default function Layout({ title, children, breadcrumb }) {
  // console.log({breadcrumb})
 

  return (
    <>
      <Head>
        <title>{title ? "Getwell " + title : "Getwell"}</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <script src="//code.tidio.co/8zv4tp3wwydqboegmwj9nfbyfbzjsi81.js" async></script>
      </Head>

      <div>
        <div >
          <header className="sticky top-0 z-50 shadow-md ">
            <Navbar breadcrumbTitle={breadcrumb} />
          </header>
          <main>
            {children}</main>
        </div>
        <footer>
          <Footer />
        </footer>
      </div>
    </>
  );
}
