import React from 'react';
import {FaPlay} from 'react-icons/fa'
import Link from 'next/link';

function BannerButtons(props) {
    return (
        <div className='xl:pl-10  pl-4'>
            <h2 className="xl:text-7xl md:text-4xl text-2xl text-white pl-12 font-bold xl:py-6 lg:py-4 py-0">{props.content?.page_title}</h2>
            {/* bug pada highlight bold di short desc karena dari API (dinamic) */}
            <p className="xl:text-2xl max-w-xl text-md text-white md:pr-4 pr-6 pl-12 font-thin text-open-sans xl:py-6 lg:py-4 py-2">{props.content?.short_descriptions}</p>

            <div className='flex justify-start pl-12 gap-3'>
                {props.content?.banner_buttons?.map((button) => (
                    <Link key={button.id} href={button.url} target="_blank" className={button.type !== 'video' ? 'bg-primary xl:py-3 py-1 xl:px-6 px-3 text-white rounded-full flex justify-center items-center gap-2 xl:text-base text-sm' : 'bg-transparent border-primary-1 rounded-full text-white flex justify-center items-center gap-2 xl:py-3 py-1 xl:px-6 px-3 xl:text-base text-sm'}>
                        {button.type === 'video' && (
                            <FaPlay />
                        )}
                        {button.title}
                    </Link>
                ))}
            </div>
        </div>
    );
}

export default BannerButtons;