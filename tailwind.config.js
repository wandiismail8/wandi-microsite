/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        'open-sans': ['"Open Sans"', 'cursive'],
        'maven-pro': ['"Maven Pro"', 'cursive'],
      },
    },
  },
  plugins: [
    require('@tailwindcss/aspect-ratio')
  ],
}